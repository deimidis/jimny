---
title: El primer viaje
date: 2020-07-03
featured_image: /images/blog/primer-viaje.jpg
image_caption: Desde el drone
excerpt: Después de algunas vueltas por las cercanías a hacer compras, por fin al Jimny le tocó conocer el polvo de la ruta.
tags:
    - blog
    - viajes
    - la Paz
---

Después de algo más de dos semanas de haber recibido al Jimny del Sur, por fin tuvimos la chance de salir a pasear lejos del asfalto (no tan lejos, fueron apenas unos 25 kilómetros), y que los neumáticos conocieran el ripio y la tierra. Esas semanas mehabían dado tiempo de revisar mapas de una excursión que fuera lo suficientemente cerca de La Paz para que podamos ir y volver en pocas horas, pero que nos permitiera pasear por las montañas.

El año pasado había hecho una excursión a una laguna saliendo por el lado de Achumani, así que busqué por esa zona adónde podíamos haber ido y encontré una cascada que parecía el lugar ideal, la *cascada Kormini*, dentro de una zona considerada Patrimonio Natural Paisajístico, llamada Serranías de Hampaturi:

<div class="mx-auto">
<iframe class="mx-auto" src='https://www.gaiagps.com/public/jKsSWXuKyjbUxNgJDyNyHcRo?embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; max-width:420px; width:100%; height: 420px;' scrolling='no' seamless='seamless'></iframe>
<p class="text-center mt-5">(según el sitio son 25.8kms y 1616 metros de altura de diferencia entre ambos puntos)</p>
</div>

Salimos a las 10, aproximadamente, después de cargar nafta. El Jimny es compatible con Android Auto, pero solo permite usar Google Maps o Waze como sistemas de mapas, y ninguno de los dos incluye los caminos que teníamos que usar. Por suerte, la primera parte del camino la conocía, más o menos, porque Osmand me quería llevar por la ruta 3, que me haría cruzar el centro de La Paz, algo que no queríamos hacer. Tardamos un tiempo en convencer a Osmand que nos mostrara el camino que queríamos.

Hasta la salida de Achumani, el camino es por calles asfaltadas, un poco rotas, pero sin mayores preocupaciones. Hasta que llegamos al segundo puente por sobre el Río Achumani, a partir de ahí fue un empedrado en no muy buen estado, y todo subida. Fue una primera prueba interesante para mi y el Jimny, acostumbrarme a manejar en montaña por subidas donde, en la mayor parte de los lugares, apenas pasa un auto.

No tuvimos grandes inconvenientes en seguir la ruta, salvo una pequeña curva muy cerrada que tanto a la ida como a la vuelta nos confundió y seguimos de largo. Y en esas cirunstancias los mapas empiezan a dar vueltas sobre sí mismos, y a veces hace difícil entender para dónde quiere que vayamos. Me sirvió para notar lo fácil que es dar vuelta con un auto tan chico como el Jimny y con dirección asistida.

Ya cerca de Lorocota la ruta empieza a ser más interesante. Por ahí todavía se ve el rostro de Evo en los carteles de las obras que hicieron: Unidades Educativas, Polideportivos y agua. De hecho, la ruta nos iba a llevar por tres grandes represas que se hicieron en los últimos años para administrar el agua que llega a La Paz. El río que bordeábamos era el Irpavi, el mismo que está a menos de una cuadra del edificio donde vivimos.

Cerca de Hampaturi vimos un camión que estaba corriendo unas piedras para poder avanzar en el camino y lo seguimos. Mi poco conocimiento de la zona y de Bolivia, me hicieron sospechar que habían dejado esas piedras de otro momento de protestas, pero que el camión solo estaba moviendo una para poder pasar. Pero no, el bloqueo tenía una razón, el camino estaba en reparaciones a tan solo 500 metros de ahí. Pensé que ya no podríamos seguir y que nuestro paseo se detenía en ese momento. Por suerte me dijeron que un poco más abajo había un desvío, exactamente donde el camión había pasado moviendo piedras. Volviendo un poco atrás pudimos retomar por ese camino que ni secundario llegaba a ser. Pasamos por un inmenso polideportivo, cerca había un estadio de pasto sintético (o eso imaginamos a lo lejos por el hermoso verde que tenía).

A partir de ahí el camino empezó a ser más fuerte, una subida en pleno ripio que el Jimny llevaba muy bien sin tener que usar la tracción en las 4 ruedas. Todo perfecto. Paramos para sacar fotos a la primera laguna/represa que vimos, y cuando quisimos reiniciar el camino, ya no le fue tan fácil al Jimny. Resbalaba y se apagaba el motor (claro que acá puede haber mucho de impericia mía). Eso si, noté las ventajas de la ayuda para arrancar en subida, hay que preocuparse de mucho menos. Con la 4x4 alta también le costó, así que pasé a la 4x4 baja y salió muy tranquilo. De ahí en más usé más tiempo la tracción en las 4 ruedas.

Por fin vimos el desvío hacia la cascada, donde el camino ya se convertía en lo mínimo necesario para un auto. Cruzamos algunas llamas que pastaban a la vera del camino, y por fin llegamos a la cascada:

![El Jimny y su orgulloso dueño](/images/blog/jimny-cascada.jpg)

Es increíble que a tan pocos kilómetros de La Paz uno pueda estar tan inmerso en la naturaleza. Si bien es algo que había aprendido el año pasado que hice varias excursiones por el día, no deja de sorprenderme. No nos cruzamos con muchas personas más, así que seguramente quedará como un buen lugar para ir a tomarse unos mates.

Disfruté mucho de manejar el Jimny, apesar de los nervios que tenía por ser el primer viaje en serio, y por cosas que aún no terminé de configurar, mi relación con el Android Auto, la pantalla y otros detalles menores. Lamentablemente no fui tan bueno grabando con el drone a la vuelta, donde lo hicimos seguir al Jimny, pero sin el botón de grabar.
