---
title: Sorata, miradores y calles pequeñas
date: 2021-02-10
featured_image: /images/blog/jimny-sorata.jpg
image_caption: La ciudad en el valle
excerpt: A unas tres horas de La Paz se encuentra esta ciudad, famosa por la caverna de los murciélagos, los miradores y su carrera de altura.
tags:
    - blog
    - viajes
    - la Paz
---

Me atrasé unos meses en subir este viaje. Ya eran finales del 2020 y, dentro de todo, la situación con el COVID parecía ser mejor, aunque había que cuidarse. Había estado en Sorata hacía muchos años, en ese momento fue el reemplazo de Coroico, adonde no podía irse. Ya se empieza a bajar de la altura de La Paz y se ven más colores y se siente la humedad.

Hay que salir por El Alto y tomar la ruta que va a Coroico hasta llegar al cruce de Huarina, y en lugar de doblar, seguir por la ruta 16. Después se cruza la ciudad de Achacachi y la ruta sigue directo hasta Sorata. 

Era un domingo, así que El Alto estaba cargado. Siempre implica un poco de stress pasar por La Ceja, no sabiendo de dónde puede aparecer un minibus o una persona. Ya cuando se pasa la última estación del teleférico azul, la ruta empieza a ser un poco más amable.

La ruta está en buen estado y el últiomo tramo es muy lindo: camino de montaña, sus curvas y su descenso. Pero la llegada no fue en un buen momento. El pueblo es chico y debíamos pasarlo por el centro hasta llegar al hotel. Y las calles empezaron a estar cerradas, incluyendo mitad de la plaza principal (por la premiación de la carrera de las alturas) y la ruta que había que seguir, por refacciones. Así que de pronto estaba en una ciudad en las que por sus calles casi cabe un auto y un cuarto, teniendo que encontrar un camino alternativo. En resumen, hice un seminario intensivo de manejo en marcha atrás.

<img class="mx-auto mb-5 w-1/2" src="/images/blog/mapa-sorata.png">

Este es el mapa de Sorata. OpenStreetMaps tiene el mapa más completo de Sorata y sus alrededores. Pero muchas esas de las que parecen calles, de pronto terminan en una escalera que el auto, a pesar de ser un 4x4, no podía bajar. Así que he metido al pobre Jimny en calles que de pronto no tenían salida, y había que volver a hacer tres o cuatro cuadras marcha atrás, en el peor de los casos en una subida.

Pregunté por un camino alternativo y me explicaron el único posible, que resultó estar cerrado por una fiesta. Después de hablar con el hotel, decidimos que era mejor almorzar en Sorata, esperar que despejen la plaza, termine la fiesta, y volver a intentar llegar.

Ya a esa altura nos habíamos dado cuenta que éramos las únicas personas en la ciudad que usábamos barbijo. No voy a decir que nos miraban como a locos, pero no debía estar muy lejos. En la plaza principal encontramos algunos restaurantes abiertos, así que pudimos ver como desarmaban la premiación mientras almorzábamos. Después de dejar las cosas en el hotel aprovechamos para ir a la Gruta de San Pedro. Mis recuerdos no eran tantos de mi anterior paso, salvo algunas imágenes del ingreso a la caverna. Claro que en estos años algo había cambiado, algunas luces más de las que recordaba y habían puestos botes a pedal en el lago. Sigue siendo interesante para visitar, sobre todo para mi que no escucho a los murciélagos que viven dentro.

Esa noche decidimos cenar en el pueblo, pero por suerte pedimos un taxi. Entre que el camino era un desastre y el nivel de alcohol en sangre de algunos conductores, era mejor no estar al frente del volante. Conseguir un taxi para volver no fue tan fácil, pero dando vueltas a la plaza finalmente conseguimos uno.

El lunes había que despertarse temprano para poder recorrer los miradores cercanos, almorzar y empezar la vuelta. El primer mirador tenía una parte difícil si uno sufre de vértigo, porque había que pasar por un camino angosto, teniendo caída de ambos lados. El video no me deja mentir, aunque también permite apreciar la ventaja con la que cuenta el perro, de tener más apoyos y un centro de gravedad más cercano al suelo :D. La vista al valle era impresionante. El camino seguía después al otro mirador, menos espectacular al no estar suspendido en el aire, pero del que se tenía mejor vista del río y las poblaciones cercanas.

Después del almuerzo en el hotel, empezamos el regreso, que incluía parar en el tercer mirador, donde estaba el cristo. Gracias a la recomendación del dueño del hotel, en lugar de hacer exactamente el camino inverso, nos desviamos en Achacachi hacia el Lago Titikaka y seguimos la ruta que lo bordeaba, paseando por algunos pueblos que no conocíamos. Nunca está de más ver al Titikaka. Acá les dejo el video:

<iframe class="mx-auto mb-5" width="560" height="315" src="https://www.youtube.com/embed/QWYPds0KVs4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

