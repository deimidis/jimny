---
title: La laguna minera
date: 2020-07-31
featured_image: /images/blog/jimny-Illimani-youtube.jpg
image_caption: El Jimny y la montaña
excerpt: Esta vez nos alejamos unos 70km de La Paz por ruta y caminos de ripio, pasando los 4700mts.
tags:
    - blog
    - viajes
    - la Paz
---
Cuando fuimos para el Cañón de las ánimas paramos en un mirador muy lindo. Pero la ruta seguía, así que busqué en GaiaGPS que  podíamos encontrar más adelante. A varios kilómetros, pasando un lugar que recordaba haber visitado el año pasado, encontré una laguna que parecía tener un camino secundario que nos podía acercar. Marqué la ruta en el mapa y esperé nuestro próximo día libre.

<div class="mx-auto mb-5">
<iframe class="mx-auto" src='https://www.gaiagps.com/public/CRemzg1PJdnVnO8HCu8HyMen?embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; max-width:420px; width:100%; height: 420px;' scrolling='no' seamless='seamless'></iframe>
</div>

El camino eran alrededor de 70 Kilómetros hasta la base del Illimani, y GaiaGPS ya me permitía ver que subiríamos y bajaríamos muchas veces. Pero la mayor parte de la ruta estaba marcada de amarillo y, por lo que sabía, eso significaba asfalto.

No sabía nada.

Apenas unos dos o tres kilómetros después del mirador, el asfalto se terminó para dejar paso al ripio por algunos cientos de metros. Después volvió el asfalto para una de las mejores partes de la ruta, en bajada y por el medio de pequeños cañones. Al poco de pasar Palca, el camino ya se convirtió definitivamente de ripio. Por momentos en la ruta solo podía pasar un vehículo tranquilamente, así que tuvimos un par de encuentros con camiones que siempre fueron resueltos con mucha diplomacia.

Sobre el final, pasando Tres Rios, debíamos estar atentos para poder tomar un desvío que, por suerte, era bastante claro. Esa parte del viaje es muy linda, en un camino de ascensos y descensos, cruzando algunos ríos, bien en medio de la nada. A lo lejos, en medio de las montañas se divisaban algunas casas que dedujimos debían ser el campamento que aparecía en el mapa. Al llegar nos dimos cuenta que era un campamento minero. Un viejo cartel decía que no podíamos avanzar más, pero decidimos que nos íbamos a arriesgar (era pleno mediodía, un sol que rajaba la tierra, difícil que alguien anduviera dando vueltas por ahí).

Pasamos una casilla vacía, sabiendo que en cualquier momento nos iban a detener para que peguemos la vuelta. Ya nos acercábamos a la laguna que era nuestro objetivo, pero siempre había una curva o una subida más. Hasta que encontramos una barrera que, aunque estaba abierta, tenía un cartel que advertía que estaba prohibido el ingreso de personas ajenas al proyecto. No se veía a nadie, así que dejamos el Jimny al costado del precipicio, dejando lugar para que pasen otros autos, y caminamos hasta la laguna.

No cruzamos a nadie, pero una laguna en medio de un campamento minero no es el sueño de un lugar donde tomar unos mates o sacar a volar el drone. Así que nos contentamos con una foto y emprendimos la vuelta hasta el Jimny. Como me había olvidado de cargar una de las baterías del drone, tenía muy poco tiempo para filmar, así que en el camino de ida fui decidiendo los lugares donde haría las tomas. En la última parada para filmar, aprovechamos y comimos las salteñas de hoja que habíamos llevado para almorzar.

Para este viaje usé el Osmand como mapa. Como no es compatible con Google Auto, lo tengo que llevar en el teléfono, e iba en un soporte comprado en RadioShack de Bolivia. Casi llegando a Palca, confundí el camino, creo que por seguir a quien venía delante y porque parecía ser más oficial, y no mirar el teléfono. Y si hay algo en lo que no parece andar muy bien el Osmand es en recalcular la ruta. Para cuando nos dimos cuenta que estábamos en un camino que no era el por el que habíamos venido, ya Osmand había decidido otra ruta. El tema es que confiaba demasiado en el auto. Al poco tiempo vimos que nos hacía seguir un camino por el medio de un cañón que, apenas si era camino y no parecía mejorar en el corto plazo. Además, al perder señal, cada tanto amagaba que teníamos que ir para lugares por donde no había ni una huella de llama.

Finalmente decidí volver sobre mis pasos hacía Palca, que al menos tenía lugares donde podríamos preguntar cómo volver. Ya en el pueblo decidimos probar con Google Maps, y nos marcó el camino de vuelta hasta donde había perdido la ruta original. En esos momentos donde comenzaba a aflorar cierta desesperación, el soporte del teléfono se rompió, se ve que no estaba preparado para caminos de aventura.

Casi siete horas después de haber salido, llegamos a casa. Más allá de que la laguna no era lo que esperábamos y del *desvío* por el cañón, el viaje fue muy divertido. Acá dejo el video del día:

<div class="mx-auto mb-5">
<iframe class="mx-auto" width="560" height="315" src="https://www.youtube.com/embed/KOhjEQmrbkc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

### Coda 1

Al salir el Jimny tenía medio tanque. En teoría debía alcanzarnos tranquilamente, por si acaso puse el modo de *autonomía* para que vaya calculando los kilómetros que podríamos recorrer. Después de este viaje pude comprobar que las subidas y bajadas no son el ecosistema ideal para la computadora de a bordo. Pasábamos de poder llegar a viajar 200 kilómetros a 40 con demasiada facilidad :D

### Coda 2

Después de cómo se quebró el soporte del teléfono me puse a buscar alguno que sea especial para 4x4. [Encontré algunos posibles candidatos](http://teamghettoracing.com/vehicles/cars/2019-jimny-jb74w/ram-mounts/), pero me parece que no se consiguen en Bolivia, así que tendré que ver cómo hago para traerlos. En ese caso, creo que voy a comprar dos, uno para el teléfono y otro para poder colgar la DJI pocket y filmar la ruta.