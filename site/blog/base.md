---
title: Hacia la base del Illimani
date: 2020-08-17
featured_image: /images/blog/base-illimani.jpg
image_caption: Caminos de montaña
excerpt: El objetivo era llegar al campamento base del Illimani. Aunque no lo logramos, fue un camino muy divertido.
tags:
    - blog
    - viajes
    - la Paz
---
Esta vez evaluamos tres opciones: a) intentar nuevamente el cañón de las ánimas por otro lugar de la ruta, b) ir a la muela del diablo y c) visitar el campamento base del Illimani, que se llama Campamento Puente Roto. Este último es el primer punto para poder escalar el Illimani (algo que no íbamos a hacer). Y aunque no se veían lagunas cercanas y gran parte de la ruta era la misma que habíamos hecho la última vez, decidimos esta última.

La ruta que nos mostraba Google Maps iba por el mismo camino del valle de las ánimas, hasta Palca, donde debíamos desviarnos por un camino secundario hasta Pinaya. Una vez pasado Palca, el camino ya era muy secundario, de ripio por montañas suaves. En Palca, hay que decirlo, no parecían estar respetando ni el distanciamiento social ni lo de no compartir botellas. Este camino, al contrario de otros que hemos hecho en los viajes anteriores, era bastante solitario.

<div class="mx-auto mb-5">
<iframe src='https://www.gaiagps.com/public/ptkTTdsATg11BXG13ksSnB17?embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; max-width:420px; width:100%; height: 420px;' scrolling='no' seamless='seamless' class="mx-auto mb-5"></iframe>
</div>

Esta es la ruta que seguimos desde Palca, llegando a Pinaya y después volviendo hacia la *carretera Palca*. Como se puede ver no logramos acercarnos tanto al Campamento Puente Roto. Tuvimos dos problemas, uno de solución monetaria que quizás usemos en otro momento y otro natural. El camino desde Pinaya estaba cerrado por una soga con candado. Nos dijeron que alguien de una casa vecina tenía la llave, pero tampoco nos dijeron exactamente cuál era la casa. Decidimos probar suerte por otro camino, que era un poco más largo. Cuando volvimos a pasar por la *"plaza" de Pinaya*, un señor nos hizo señas, a pesar de mi llamado de atención por no estar con el tapabocas, paramos. Resultó ser un guía de montaña que, al vernos claramente extranjeros, decidió ofrecer su servicios. Quedamos en poder arreglar con él para subir hasta la cima del Illimani (aunque dudo que lo hagamos). Nos dijo que el campamento estaba cerrado, y que para pasar la soga debíamos dejar una pequeña colaboración de 50 bolivianos (el día que quisiéramos subir).

No nos dimos por vencidos y decidimos intentarlo por el otro camino que se veía en el Osmand. Aprovechamos para comer las salteñas que habíamos llevado y durante parte de ese camino usamos el drone para filmar el paseo del Jimny. Al poco tiempo entendimos por qué el camino "oficial" era el otro, cada vez se parecía menos a un camino, la huella apenas podía adivinarse. Por suerte, de un lado estaba la montaña y del otro el precipicio, así que no había mucha duda de por dónde seguía. Y de pronto encontramos el resultado de un derrumbe que impedía que siguiéramos. Por si acaso miré cuántas piedras había, con la leve ilusión de poder correrlas, pero eran demasiados metros de piedras como para intentarlo. Así que dimos por terminado el paseo. Eso si, nos quedó una parte muy linda para filmar con el drone y seguir a unas llamas que se negaban a abandonar el camino.

Creo que la próxima vez ya iremos para otros lados, quizás la Muela del Diablo, quizás ya cruzar El Alto rumbo a las otras montañas de la Cordillera.

<div class="mx-auto mb-5">
<iframe width="560" height="315" src="https://www.youtube.com/embed/zutiPZM6hwo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="mx-auto mb-5"></iframe>
</div>