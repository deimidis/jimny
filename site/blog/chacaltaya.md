---
title: Chacaltaya, un glaciar desaparecido
date: 2020-09-28
featured_image: /images/blog/chacaltaya.jpg
image_caption: El centro de esquí
excerpt: En el 2010 desapareció el glaciar, y con él, el centro de esquí. Subimos hasta los 5200 msm en un viaje que incluyó lluvia, nieve y sol.
tags:
    - blog
    - viajes
    - la Paz
---
Desde que me enteré que había existido un centro de ski en las cercanías de La Paz, que ahora estaba abandonado pero se podía visitar, tomé la decisión de conocerlo. Había ido para esa zona el año pasado, pero sin conocer la historia, y tomando otro camino. Salimos temprano porque aunque es cerca (32 kilómetros), había que cruzar el centro de La Paz un lunes. El centro de La Paz no es el lugar más cómodo para manejar, así que tenía que estar atento.

Un descubrimiento de los primeros pasos por el centro de La Paz es que Google Maps no parece tener actualizados los sentidos de algunas calles y se empeñó en hacer que hiciera una cuadra a contramano, y eso nos llevó a un desvío por una zona más transitada. Pero fuera de los nervios, avanzamos bien hasta la autopista y la salida posterior al camino que nos llevaría hasta el Chacaltaya. Algo interesante de La Paz es que en cualquier momento, una calle asfaltada se convierte en un empedrado y a los 50 metros ya es ripio, en medio de la montaña.

En el viaje de ida, pasamos por sectores donde la ruta estaba "cerrada" pero aún pasaban autos. Con la lluvia no había nadie trabajando. La sorpresa llegaría a la vuelta.

<div>
<iframe src='https://www.gaiagps.com/public/dD9fyIR6znVY5Or4ek8z27kt?embed=True' style='border:none; overflow-y: hidden; background-color:white; min-width: 320px; max-width:420px; width:100%; height: 420px;' scrolling='no' seamless='seamless' class='mx-auto mb-5'></iframe>
</div>

El clima no era el mejor, y subíamos en altura, así que cada llovizna era más fría y espesa. Al poco tiempo ya eran pequeños copos de nieve que iban blanqueando el camino. Era la primera vez que salía con tres baterías para el drone. La idea era usar una para probar una nueva app, [Dronelink](https://app.dronelink.com/). Esta aplicación permite automatizar los vuelos, dándole órdenes al drone y que uno pueda relajarse y ver cómo sigue su plan de vuelo. Por el clima, decidí hacerlo a la vuelta.

Empezamos a ver lagunas, desde la ruta que ascendía, y a pesar de la llovizna/nieve paramos a hacer un primer vuelo sobre las lagunas: las primeras imágenes que se ven en el video que subí a YouTube. El drone se comportó muy bien a pesar del frío, la altura y nieve. Ya nos faltaba poco para llegar hasta el centro de ski, así que seguimos para poder almorzar ahí.

Llegamos a las casas y la lluvía/nieve no paraba, así que finalmente almorzamos en el Jimny, quedándonos con ganas de subir la montaña por el camino que se ve en el video. A la vuelta aproveché para las tomas de seguimiento del Jimny. Ya en la bajada empezó a mejorar el clima, con las primeras veces en que el sol asomaba. Llegó el momento de probar *Dronelink*. Ahí aprendí que mi afán por no darle permisos que creo que no necesita se me volvían en contra. La aplicación no se conectaba con el drone, así que finalmente lo volé manualmente.

Al volver a la ruta que estaban arreglando, nos dimos cuenta que ahora si había máquinas trabajando, así que fue imposible volver por el mismo camino, tuvimos que usar el *Osmand* para conocer un camino nuevo que nos llevó por lugares que no hubiera viajado en otras circunstancias, en teoría para ahorrar algunos minutos. Pero finalmente llegamos nuevamente a la autopista a El Alto, y pasamos bastante bien por el centro de La Paz, a pesar de ser hora pico.

<iframe width="560" height="315" src="https://www.youtube.com/embed/mK_gYdJEkvY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class='mx-auto mb-5'></iframe>

Gracias a mi amigo Nacho Castro, tomaron el video para escribir esta [buena historia](https://www.carbono.news/recursos-naturales/ni-rastro-asi-luce-hoy-chacaltaya-en-bolivia-donde-una-vez-hubo-un-glaciar/) sobre Chacaltaya y los efectos del cambio climático en las pistas de ski.

Esa ruta será nuevamente visitada, ya que por ahí se va hasta el Huayna Potosí, y también se podían visitar desde más cerca las lagunas.