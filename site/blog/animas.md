---
title: El cañón de las ánimas
date: 2020-07-20
featured_image: /images/blog/jimny-animas.jpg
image_caption: El Illimani en el horizonte
excerpt: Un gran paseo por el cañón de las ánimas, a tan solo 10 minutos de La Paz.
tags:
    - blog
    - viajes
    - la Paz
---

Dos semanas después de la primera salida, tuve un nuevo día libre en el trabajo, una nueva oportunidad de hacer un viaje con el Jimny. Estuve buscando en las cercanías con los diferentes mapas y aplicaciones y encontré un circuito en wikiloc que parecía prometedor, pasear por el medio del cañón de las ánimas. Una vez que decidí la ruta, era sólo cuestión de esperar el día, y rogar para que el camino estuviera despejado.

Lo bueno de una ciudad con un clima predecible, es que en plena temporada seca, era mucha mala suerte que nos tocara un día con lluvia. El año pasado, casi ni bien llegado a La Paz, había ido junto a Eliana y una de sus amigas a pasear por el Valle de las ánimas. Era época de lluvias, y fuimos con el Puma Katari y taxi. Nos costó llegar, y en medio del recoriido nos agarró una tormenta que nos obligó a refugiarnos en una casa en construcción por unos cuarenta minutos. Pero igual pude hacer volar el drone y comimos una rica comida en una parada de minibuses. 

Esta vez la ruta elegida era un poco antes. Llevaba el recorrido en los dos teléfonos, el que estaba conectado al Android Auto del Jimny, y el que iba con el Osmand y Wikiloc, que tiene mejores mapas que los de Google (sigo odiando que Android Auto no permita otros mapas que no sean Google Maps y Waze).

Llegar hasta el inicio de nuestro camino fue fácil, solo había que seguir la misma avenida. Una vez en el punto **1** del mapa que se ve debajo, empezaba la aventura. 

<div class="flex justify-center mb-6">
    <img class="text-center" width= 498 src="/images/blog/animas-gaia-2.png" alt="El mapa de la ruta" />
</div>

El camino empezaba en subida y en mal estado. A los pocos metros, vimos una cadena que obstruía el camino, con un enorme candado. Una pareja de personas mayores estaba trabajando una huerta al costado del río, y les pregunté si se podía seguir avanzando. Ellos no tenían llave del candado. Era momento de dar la vuelta. Sabía que dónde había estado el año pasado no estaba lejos, había una laguna (aunque no me habías gustado mucho la vez anterior), así que pensé en ir para allá, pero después recordé que el punto **2** del mapa permitía otro ingreso, decidí que había que probar esa chance.

A los pocos metros de abandonar el asfalto, encontramos un desprendimiento de rocas que casi bloqueaban el camino. Estaban lo suficientemente espaciadas como para al menos intentarlo. Puse la tracción en las cuatro ruedas y lo intenté, pero se quedó después de pasar algunas piedras, así que le llegó el turno a la tracción cuatro por cuatro en baja. Y ahí pasamos sin problemas. Una gran ventaja del 4x4 y del Jimny y su altura con el suelo. Después de esas piedras, el camino era un ripio muy transitable que, de a poco, iba entrando al cañón de las ánimas.

## El primer vuelo del drone

Paramos a los pocos metros para probar nuevamente si el drone podía seguir al Jimny. Era un espacio amplio así que habría pocos riesgos de que chocara contra algo. La idea es simple, levanto vuelo, programo la opción de *seguimiento*, hago clic en *grabar*, dejó el control remoto del drone, y empiezo a manejar. Como el camino no era fácil, la velocidad no iba a ser un problema. Pero aún no entiendo cuándo me sigue y cuándo no. En este primer intento, la cámara nos siguió un poco, pero casi enseguida perdió el rastro. Así que lo dejé en una posición donde pudiera hacer una toma de varios metros, y filmé eso. Después, cuando lo traía hasta nuestra posición, el teléfono perdió la conexión con el control remoto y empezó la parte divertida.

Escuchaba el zumbido del drone cerca, así que no me preocupé mucho. Mientras reiniciaba el teléfono, el drone decidió que había pasado mucho tiempo sin conexión y emprendió la vuelta al lugar de origen. Cada vez lo escuchaba más lejos y ya no lo veía. Así que decidí caminar hasta la posición desde donde había despegado que, por suerte, no era tan lejos. Cuando llegué lo vi suspendido a tan solo un metro del suelo, listo para aterrizar. Volver fue otra historia, esos pocos metros, eran todos en subida y tardé mucho tiempo en recuperarme. Después, seguimos el camino.

![El Jimny en el cañón](/images/blog/jimny-animas-2.jpg)

## Los límites del 4x4

A unos 500 metros más adelante el camino estaba cortado. El cauce de un río (que ni un hilo de agua tenía en esta temporada seca) estaba en reparaciones, posiblemente para que el camino pudiera ser usado en las temporadas de lluvia. Pero nos hacía imposible seguir con el Jimny. Así que decidimos caminar un rato por el camino que subía por la ladera más cercana. Un poco más allá se veía como el camino apto para autos seguía. El sol del mediodía era fuerte, pero fue divertido caminar entre ese paisaje desértico.

Decidimos volver al auto, aún no sabiendo si sería el lugar para tomar unos mates y comer los chipás/cuñapés que Lis había hecho el día anterior. Ahora si nos encaminamos a la laguna de las ánimas, aunque mi recuerdo no hubiera sido el mejor. El tiempo no la había modificado mucho, así que no llegamos a estar ni cinco minutos, antes de seguir el camino que terminó en un espacio al costado de la ruta desde donde se podía ver el Illimani (la foto que hace de portada al post).

Edité un video que muestra el paseo

<div class="flex justify-center mb-6">
<iframe width="560" height="315" src="https://www.youtube.com/embed/AeNe8VUbbYY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Y [acá se puede ver una foto en 360º](https://www.skypixel.com/photo360s/valle-de-las-nimas-la-paz) del Cañón de las ánimas (skypixel, el sitio de DJI no deja incluirla en sitios web).