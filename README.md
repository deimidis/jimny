[![pipeline status](https://gitlab.com/deimidis/jimny/badges/master/pipeline.svg)](https://gitlab.com/deimidis/jimny/commits/master)

# Jimny del Sur

Basado en el proyecto Skeleventy, hecho con Eleventy y TailwindCSS. Repositorio [Skeleventy](https://skeleventy.netlify.com/).

En este caso, en lugar de alojarlo en Netlify, estoy queriendo usarlo con Gitlab Pages

## Pasos que fui dando

- [x] Clonar el repositorio de [github](https://github.com/josephdyer/skeleventy)
- [x] Borrar todo las referencias al repositorio de github
- [x] Crear un proyecto nuevo en mi usuario de gitlab. Al crearlo vacío hay referencias de cómo hacer los commits de un directorio existente. Seguí esas instrucciones.
- [x] Modificaciones en el sitio para el diseño que estaba pensando
- [x] Configurar el gitlab-ci.yml para que haga la compilación cada vez que hago un commit.
- [x] Configurar el dominio que compré jimnydelsur.me

El sitio ya se encuentra con su propio dominio en [jimnydelsur.me](jimnydelsur.me)